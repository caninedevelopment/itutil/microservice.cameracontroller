﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using MjpegProcessor;
using Accord.Vision.Detection;
using Accord.Vision.Detection.Cascades;
using CameraHttpTrigger.DTO;
using ITUtil.Common.DTO;

namespace CameraHttpTrigger
{
    public class Logic
    {
        public List<TriggerTypeData> AllTriggerTypes;
        public DateTime lastIMGCheck;
        public string id;
        private int counter = 1;
        private Bitmap lastImage;

        public List<Trigger> CheckImageForTrigger(Bitmap image)
        {
            List<Trigger> triggeredTriggers = new List<Trigger>();

            foreach(TriggerTypeData trig in AllTriggerTypes)
            {
                if(trig.detectionType == TriggerType.FaceDetection)
                {
                   // int facedetectionScale = 2;
                    using (Bitmap bm = new Bitmap(image, new Size(400, 300)))
                    {
                        //facedetectionScale = image.Width / bm.Width;
                        
                        //Dictionary<Double, Rectangle[]> detectionArea = new Dictionary<double, Rectangle[]>();
                        foreach (Trigger t in trig.triggerData)
                        {
                            Rectangle[] detectedArea = FaceDetecter(bm, t.treshold);

                            if (detectedArea.Length > 0)
                            {
                                triggeredTriggers.Add(t);
                            }

                            //if (!detectionArea.ContainsKey(t.treshold))
                            //{
                            //    Rectangle[] detectedArea = FaceDetecter(bm, t.treshold);

                            //    detectionArea.Add(t.treshold, detectedArea);
                            //}
                        }

                        //if (detectionArea.Count > 0)
                        //{
                        //for (int t = 0; t < trig.triggerData.Count; t++)
                        //{
                        //    //var detectAreal = trig.triggerData[t].polygon.Convert2Area();
                        //    //int x;
                        //    //int y;
                        //    //int width;
                        //    //int hight;

                        //    //if (detectAreal.topLeftX != 0)
                        //    //{
                        //    //    x = detectAreal.topLeftX / facedetectionScale;
                        //    //}
                        //    //else
                        //    //{
                        //    //    x = 0;
                        //    //}
                        //    //if (detectAreal.topLeftY != 0)
                        //    //{
                        //    //    y = detectAreal.topLeftY / facedetectionScale;
                        //    //}
                        //    //else
                        //    //{
                        //    //    y = 0;
                        //    //}
                        //    //if (detectAreal.width != 0)
                        //    //{
                        //    //    width = detectAreal.width / facedetectionScale;
                        //    //}
                        //    //else
                        //    //{
                        //    //    width = 0;
                        //    //}
                        //    //if (detectAreal.height != 0)
                        //    //{
                        //    //    hight = detectAreal.height / facedetectionScale;
                        //    //}
                        //    //else
                        //    //{
                        //    //    hight = 0;
                        //    //}

                        //    //Rectangle rec = new Rectangle(x, y, width, hight);

                        //    //for (int r = 0; r < detectionArea[trig.triggerData[t].treshold].Length; r++)
                        //    //{
                        //    //    if (detectionArea[trig.triggerData[t].treshold][r].IntersectsWith(rec))
                        //    //    {
                        //    //        triggeredTriggers.Add(trig.triggerData[t]);
                        //    //        r = detectionArea[trig.triggerData[t].treshold].Length;
                        //    //        t = trig.triggerData.Count;
                        //    //        break;
                        //    //    }
                        //    //}
                        //}
                        //}
                    }
                }
                else if(trig.detectionType == TriggerType.HumanDetection)
                {
                    //Not done
                    if (HumanDetected(image))
                    {
                        //make so it only adds one
                        foreach (Trigger t in trig.triggerData)
                        {
                            triggeredTriggers.Add(t);
                        }
                    }
                }
                else if (trig.detectionType == TriggerType.VechileDetection)
                {
                    //Not done
                    if (VechileDetected(image))
                    {
                        //make so it only adds one
                        foreach (Trigger t in trig.triggerData)
                        {
                            triggeredTriggers.Add(t);
                        }
                    }
                }
            }

            return triggeredTriggers;
        }
        public void DoAction(List<Trigger> triggers, MemoryStream ms)
        {
            if (triggers != null && triggers.Count > 0)
            {
                Console.Write("+");
                var callback = new EventCallbackDto() { id = Guid.NewGuid(), route = "cameracontroller.v1.eventCallback" };
                var bytes = ms.ToArray();
                var triggerevent = new TriggerEvent() { cameraId = Program.cameraSettings.id, onTriggers = triggers, base64Frame = Convert.ToBase64String(bytes) };
                var baseDir = Directory.GetCurrentDirectory() + @"\events\";
                if (Directory.Exists(baseDir) == false)
                {
                    Directory.CreateDirectory(baseDir);
                }

                var result = ReturnMessageWrapper.CreateResult(true, new MessageWrapper(new TokenData()
                {
                    claims = new MetaData[] { },
                    tokenId = Guid.Empty,
                    userId = Guid.Empty,
                    validUntil = DateTime.Now
                },
                DateTime.Now,
                Guid.Empty,
                string.Empty,
                string.Empty,
                string.Empty
                ),
                triggerevent,
                ITUtil.Common.DTO.LocalizedString.OK);

                File.WriteAllText(baseDir + callback.id.ToString() + ".event", Newtonsoft.Json.JsonConvert.SerializeObject(result));
                ITUtil.Common.Utils.RabbitMQ.EventClient.Instance.RaiseEvent("cameracontroller.v1.ontrigger",new ReturnMessageWrapper(string.Empty, string.Empty, ITUtil.Common.Utils.MessageDataHelper.ToMessageData(callback)));
            } else
            {
                Console.Write("-");
            }
        }
        private bool MotionDetected(Bitmap image)
        {
            bool res = false;

            double sensetevity = 0;
            //se om der er et last image før det køres
            //se pixelforskel og se i forhold til sensetevity

            return res;
        }
        private bool HumanDetected(Bitmap image)
        {
            bool res = false;

            //mangler alt

            return res;
        }
        public Rectangle[] FaceDetecter(Bitmap bm, double threshold)
        {
            HaarCascade cascade = new FaceHaarCascade();
            HaarObjectDetector detector = new HaarObjectDetector(cascade, 15);

            detector.SearchMode = ObjectDetectorSearchMode.Average;
            detector.ScalingMode = ObjectDetectorScalingMode.SmallerToGreater;
            detector.ScalingFactor = (float)(1.1 + (0.4 * threshold));
            detector.Suppression = 2;

            Rectangle[] objects = detector.ProcessFrame(bm);

            return objects;
        }
        private bool VechileDetected(Bitmap image)
        {
            bool res = false;

            //mangler alt

            return res;
        }

        public static DateTime lastEvent = DateTime.MinValue;
        public void ImageCheck(object sender, FrameReadyEventArgs e)
        {
            if (DateTime.Now > lastEvent.AddMilliseconds(Program.cameraSettings.triggerSettings.triggerDelayInMiliseconds))
            {

                MemoryStream ms = new MemoryStream(e.FrameBuffer);
                Bitmap image = new Bitmap(ms);

                List<Trigger> triggers = CheckImageForTrigger(image);

                DoAction(triggers, ms);
                if (triggers != null && triggers.Count > 0)
                {
                    lastEvent = DateTime.Now;
                }

                if (lastImage != null)
                {
                    lastImage.Dispose();
                    lastImage = image;
                }
                else
                {
                    lastImage = image;
                }

                counter++;
            }
        }
    }
}
