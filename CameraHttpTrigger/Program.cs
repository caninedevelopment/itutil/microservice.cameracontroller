﻿using System;
using System.IO;
using System.Collections.Generic;
using MjpegProcessor;

namespace CameraHttpTrigger
{
    class Program
    {
        public static CameraSettings cameraSettings;
        static void Main(string[] args)
        {
            Logic logic = new Logic();

            if(args.Length >= 1 && !string.IsNullOrEmpty(args[0]) /*&& !string.IsNullOrEmpty(args[1])*/)
            {
                //string ipURL = args[0];
                string camID = args[0];

                string file = Directory.GetCurrentDirectory() + "\\CameraHttpTrigger\\" + camID + ".txt";
                logic.id = camID;
                string data = "";
                using (StreamReader sr = new StreamReader(file))
                {
                    data = sr.ReadToEnd();
                }

                cameraSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<CameraSettings>(data);
                logic.AllTriggerTypes = new List<TriggerTypeData>();

                foreach (Trigger t in cameraSettings.camTriggerData) //opdeler triggertyperne
                {
                    bool added = false;
                    for (int i = 0; i < logic.AllTriggerTypes.Count; i++)
                    {
                        if (logic.AllTriggerTypes[i].detectionType == t.detectionType)
                        {
                            added = true;
                            logic.AllTriggerTypes[i].triggerData.Add(t);
                            i = logic.AllTriggerTypes.Count;
                        }
                    }
                    if (!added)
                    {
                        TriggerTypeData TTD = new TriggerTypeData();
                        TTD.triggerData = new List<Trigger>();
                        TTD.detectionType = t.detectionType;
                        TTD.triggerData.Add(t);
                        logic.AllTriggerTypes.Add(TTD);
                    }
                }

                logic.lastIMGCheck = DateTime.Now;


                RunDecoder(logic, cameraSettings.url.uri, cameraSettings.url.username, cameraSettings.url.password);


            }
        }
        public static void RunDecoder(Logic logic, string uri, string username, string password)
        {
            MjpegDecoder decoder = new MjpegDecoder();
            decoder.FrameReady += logic.ImageCheck;
            Console.WriteLine("Starting " + cameraSettings.url.uri);

            var res = decoder.ParseStream(new Uri(cameraSettings.url.uri), cameraSettings.url.username, cameraSettings.url.password);

            if (res != null)
            {
                Console.WriteLine(res.Message);
                if (res is System.Net.WebException)
                {
                    var response = ((System.Net.WebException)res).Response as System.Net.HttpWebResponse;
                    switch (response.StatusCode)
                    {
                        case System.Net.HttpStatusCode.Ambiguous:
                        case System.Net.HttpStatusCode.BadGateway:
                        case System.Net.HttpStatusCode.BadRequest:
                        case System.Net.HttpStatusCode.Conflict:
                        case System.Net.HttpStatusCode.Forbidden:
                        case System.Net.HttpStatusCode.HttpVersionNotSupported:
                        case System.Net.HttpStatusCode.MethodNotAllowed:
                        case System.Net.HttpStatusCode.MisdirectedRequest:
                        case System.Net.HttpStatusCode.MovedPermanently:
                        case System.Net.HttpStatusCode.NetworkAuthenticationRequired:
                        case System.Net.HttpStatusCode.NotFound:
                        case System.Net.HttpStatusCode.NotImplemented:
                        case System.Net.HttpStatusCode.PermanentRedirect:
                        case System.Net.HttpStatusCode.ProxyAuthenticationRequired:
                        case System.Net.HttpStatusCode.RequestUriTooLong:
                        case System.Net.HttpStatusCode.Unauthorized:
                            Console.WriteLine("Stop stream!");
                            decoder.StopStream();
                            return;
                        default:
                            break;
                    }
                }
                System.Threading.Thread.Sleep(5000);
                Console.WriteLine("Rerun....");
                RunDecoder(logic, uri, username, password);//restart itself...
            }
            else
            {
                Console.WriteLine("Stop stream!");
                decoder.StopStream();
            }
        }
    }
}
