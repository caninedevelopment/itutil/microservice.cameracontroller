using ITUtil.Common.DTO;
using System;
using System.Collections.Generic;

namespace Monosoft.Service.CameraController.v1.DTO
{
    public class CameraSettings
    {
        public Guid id { get; set; }
        public ResponseStreamUri url { get; set; }
        public TriggerSetting triggerSettings { get; set; }

        public List<Trigger> camTriggerData { get; set; }
    }


    public class TriggerSetting
    {
        public int secondsBeforeTriggerInFeedMode { get; set; }
        public int secondsAfterTriggerInFeedMode { get; set; }
        public int triggerDelayInMiliseconds { get; set; }
    }

}
