﻿using System.Collections.Generic;


namespace CameraHttpTrigger
{
    public class TriggerTypeData
    {
        public TriggerType detectionType { get; set; }
        public List<Trigger> triggerData { get; set; }
    }
}
