namespace Monosoft.Service.CameraController.v1
{
    using ITUtil.Common.Config;
    using Monosoft.Service.CameraController.v1.DTO;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    
    public class Logic
    {
        DBContext db = null;
        public Dictionary<System.Guid, Process> CameraProcesses = new Dictionary<System.Guid, Process>(); 

        public Logic()
        {
            var settings = GlobalRessources.getConfig().GetSetting<SQLSettings>();
            db = new DBContext(settings);
            //TODO: DOES NOT WORK!! (FIX!)
            //foreach (var cam in db.GetAll())
            //{
            //    StartCameraFeed(cam);
            //}
        }

        ~Logic()
        {
            foreach(var process in CameraProcesses.Values)
            {
                process.Close();
            }
        }

        public void SetSettings(CameraSettings camera)
        {
            db.NewSettings(camera);
            StartCameraFeed(camera);
        }
        public CameraSettings GetSettings(CameraRequest settingReq)
        {
            CameraSettings settings = db.GetSettingsFromId(settingReq.camID);
            return settings;
        }
        public bool StartCameraFeed(CameraSettings camera) //send alle triggers der h�re til cameraet med
        {
            bool camStart = true;

            if (CameraProcesses.ContainsKey(camera.id))
            {//kill old instance....
                CameraProcesses[camera.id].Close();
                CameraProcesses.Remove(camera.id);
            }

            if (camera.camTriggerData == null || camera.camTriggerData.Count == 0)
            {
                camStart = false;
            }

            if (camStart)
            {
                

                Camera cam = new Camera();
                Process process = new Process();
                string command = Directory.GetCurrentDirectory() + "\\CameraHttpTrigger\\CameraHttpTrigger.dll" + " " + camera.id.ToString();
                string filepath = Directory.GetCurrentDirectory() + "\\CameraHttpTrigger\\" + camera.id.ToString() + ".txt";
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(camera);
                File.WriteAllText(filepath, json);
                process.StartInfo = new ProcessStartInfo("dotnet");
                process.StartInfo.Arguments = command;
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.CreateNoWindow = false;
                cam.camID = camera.id;
                cam.camAplication = process;
                cam.camAplication.Start();

                CameraProcesses.Add(camera.id, process);
            }

            return camStart;
        }
    }
}