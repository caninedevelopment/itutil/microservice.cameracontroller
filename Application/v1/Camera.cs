using Monosoft.Service.CameraController.v1.DTO;
using System;
using System.Diagnostics;

namespace Monosoft.Service.CameraController
{
    public class Camera
    {
        public Guid camID { get; set; }
        public Process camAplication { get; set; }
        public CameraSettings settings { get; set; }
    }
}
