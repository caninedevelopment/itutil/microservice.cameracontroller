﻿using Monosoft.Service.CameraController.v1.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.CameraController.v1.Database
{
    public class CameraController
    {
        public Guid  Id {get;set;}
        public CameraSettings CameraSettings { get;set;}
    }
}
