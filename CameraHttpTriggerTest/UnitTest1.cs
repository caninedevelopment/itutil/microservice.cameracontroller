using NUnit.Framework;
using CameraHttpTrigger;
using MjpegProcessor;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Drawing;
using System;
using System.Threading;

namespace Tests
{
    [TestFixture]
    public class CameraTesting
    {
        string testIPURL = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";
        [Test]
        public void CameraConnectTest()
        {
            bool parse = false;

            MjpegDecoder decoder = new MjpegDecoder();
            decoder.ParseStream(new Uri(testIPURL), "root", "1234");

            while (decoder.CurrentFrame == null)
            {

            }

            if (decoder.CurrentFrame != null)
            {
                parse = true;
            }

            decoder.StopStream();

            Assert.IsTrue(parse);
        }
    }

    [TestFixture]
    public class LogicTesting
    {
        Logic logic = new Logic();
        string testIPURL = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";
        string camID;

        //public void Setup(TriggerData td, string camid)
        //{
        //    camID = camid;

        //    logic.AllTriggerTypes = new List<TriggerTypeData>();

        //    foreach (Trigger t in td.camTriggerData) //opdeler triggertyperne
        //    {
        //        bool added = false;
        //        for (int i = 0; i < logic.AllTriggerTypes.Count; i++)
        //        {
        //            if (logic.AllTriggerTypes[i].detectionType == t.detectionType)
        //            {
        //                added = true;
        //                logic.AllTriggerTypes[i].triggerData.Add(t);
        //                i = logic.AllTriggerTypes.Count;
        //            }
        //        }
        //        if (!added)
        //        {
        //            TriggerTypeData TTD = new TriggerTypeData();
        //            TTD.triggerData = new List<Trigger>();
        //            TTD.detectionType = t.detectionType;
        //            TTD.triggerData.Add(t);
        //            logic.AllTriggerTypes.Add(TTD);
        //        }
        //    }
        //}
        [Test]
        public void FaceDetectionNoTest()
        {
            bool res = false;

            Bitmap TestImg = new Bitmap(@".\TestImages\ForestNoRes800x600.jpeg");

            Rectangle[] rec = logic.FaceDetecter(TestImg, 0.0215);

            if(rec.Length == 0)
            {
                res = true;
            }

            Assert.IsTrue(res);
        }
        [Test]
        public void FaceDetectionYesTest()
        {
            bool res = false;

            Bitmap TestImg = new Bitmap(@".\TestImages\ForestYesRes800x600.jpeg");

            Rectangle[] rec = logic.FaceDetecter(TestImg, 0.0215);

            if (rec.Length == 1)
            {
                res = true;

                foreach (Rectangle r in rec)
                {
                    int hight = r.Height;
                    int with = r.Width;
                    int X = r.X;
                    int Y = r.Y;

                    for (int x = X; x < X + with; x++)
                    {
                        for (int y = Y; y < Y + hight; y++)
                        {
                            if (x == X || y == Y || x == X + with - 1 || y == Y + hight - 1)
                            {
                                TestImg.SetPixel(x, y, Color.Red);
                            }

                        }
                    }
                }

                TestImg.Save(@".\TestImages\ResultForestYesRes800x600.jpeg");
            }

            Assert.IsTrue(res);
        }
        [Test]
        public void FaceDetectionYesRangesTest()
        {
            bool res = false;

            Bitmap TestImg = new Bitmap(@".\TestImages\ForestRangeRes800x600.jpeg");

            Rectangle[] rec = logic.FaceDetecter(TestImg, 0.0215);

            if (rec.Length == 6)
            {
                res = true;
            }

            foreach (Rectangle r in rec)
            {
                int hight = r.Height;
                int with = r.Width;
                int X = r.X;
                int Y = r.Y;

                for (int x = X; x < X + with; x++)
                {
                    for (int y = Y; y < Y + hight; y++)
                    {
                        if (x == X || y == Y || x == X + with - 1 || y == Y + hight - 1)
                        {
                            TestImg.SetPixel(x, y, Color.Red);
                        }
                    }
                }
            }

            TestImg.Save(@".\TestImages\ResultForestRangeRes800x600.jpeg");

            Assert.IsTrue(res);
        }
        //[Test]
        //public void CheckImageForTriggersTest1TrigedTrigger()
        //{
        //    bool res = false;

        //    TriggerData td = new TriggerData();
        //    td.camID = Guid.NewGuid();
        //    td.camTriggerData = new List<Trigger>();

        //    Trigger t = new Trigger();
        //    t.detectionType = TriggerType.FaceDetection;
        //    t.polygon = new Polygon() { Points = new List<CameraHttpTrigger.Point>() { new CameraHttpTrigger.Point() { x = 0, y = 0 }, new CameraHttpTrigger.Point() { x = 400, y = 600 } } };
        //    t.treshold = 0.0215;

        //    td.camTriggerData.Add(t);

        //    Setup(td, td.camID.ToString());
        //    Bitmap TestImg = new Bitmap(@".\TestImages\ForestRangeRes800x600.jpeg");

        //    List<Trigger> triged = logic.CheckImageForTrigger(TestImg);

        //    if (triged.Count == 1)
        //    {
        //        res = true;
        //    }

        //    Assert.IsTrue(res);
        //}
        //[Test]
        //public void CheckImageForTriggersTestNoTriggered()
        //{
        //    bool res = false;

        //    TriggerData td = new TriggerData();
        //    td.camID = Guid.NewGuid();
        //    td.camTriggerData = new List<Trigger>();

        //    Trigger t = new Trigger();
        //    t.detectionType = TriggerType.FaceDetection;
        //    t.polygon = new Polygon() { Points = new List<CameraHttpTrigger.Point>() { new CameraHttpTrigger.Point() { x = 0, y = 0 }, new CameraHttpTrigger.Point() { x = 400, y = 250 } } };
        //    t.treshold = 0.0215;

        //    td.camTriggerData.Add(t);

        //    Setup(td, td.camID.ToString());
        //    Bitmap TestImg = new Bitmap(@".\TestImages\ForestRangeRes800x600.jpeg");

        //    List<Trigger> triged = logic.CheckImageForTrigger(TestImg);

        //    if (triged.Count == 0)
        //    {
        //        res = true;
        //    }

        //    Assert.IsTrue(res);
        //}
        //[Test]
        //public void CheckImageForTriggersTestMultiTrigger()
        //{
        //    bool res = false;

        //    TriggerData td = new TriggerData();
        //    td.camID = Guid.NewGuid();
        //    td.camTriggerData = new List<Trigger>();

        //    Trigger t1 = new Trigger();
        //    t1.detectionType = TriggerType.FaceDetection;
        //    t1.polygon = new Polygon() { Points = new List<CameraHttpTrigger.Point>() { new CameraHttpTrigger.Point() { x = 0, y = 0 }, new CameraHttpTrigger.Point() { x = 400, y = 600 } } };
        //    t1.treshold = 0.0215;

        //    Trigger t2 = new Trigger();
        //    t2.detectionType = TriggerType.FaceDetection;
        //    t2.polygon = new Polygon() { Points = new List<CameraHttpTrigger.Point>() { new CameraHttpTrigger.Point() { x = 0, y = 0 }, new CameraHttpTrigger.Point() { x = 400, y = 600 } } };
        //    t2.treshold = 0.1;

        //    Trigger t3 = new Trigger();
        //    t3.detectionType = TriggerType.FaceDetection;
        //    t3.polygon = new Polygon() { Points = new List<CameraHttpTrigger.Point>() { new CameraHttpTrigger.Point() { x = 0, y = 0 }, new CameraHttpTrigger.Point() { x = 400, y = 600 } } };
        //    t3.treshold = 0.03;

        //    td.camTriggerData.Add(t1);
        //    td.camTriggerData.Add(t2);
        //    td.camTriggerData.Add(t3);

        //    Setup(td, td.camID.ToString());
        //    Bitmap TestImg = new Bitmap(@".\TestImages\ForestRangeRes800x600.jpeg");

        //    List<Trigger> triged = logic.CheckImageForTrigger(TestImg);

        //    if (triged.Count == 1)
        //    {
        //        res = true;
        //    }

        //    Assert.IsTrue(res);
        //}
        ////multi with difrent rules
        //[Test]
        //public void ImageCheckTest()
        //{
        //    bool res = true;

        //    logic.id = Guid.NewGuid().ToString();

        //    TriggerData td = new TriggerData();
        //    td.camID = Guid.NewGuid();
        //    td.camTriggerData = new List<Trigger>();

        //    Trigger t = new Trigger();
        //    t.detectionType = TriggerType.FaceDetection;
        //    t.polygon = new Polygon() { Points = new List<CameraHttpTrigger.Point>() { new CameraHttpTrigger.Point() { x = 0, y = 0 }, new CameraHttpTrigger.Point() { x = 400, y = 600 } } };

        //    td.camTriggerData.Add(t);

        //    Setup(td, td.camID.ToString());
        //    Bitmap TestImg = new Bitmap(@".\TestImages\ForestRangeRes800x600.jpeg");

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        TestImg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

        //        logic.ImageCheck(this, new FrameReadyEventArgs(ms.ToArray()));
        //    }

        //    Assert.IsTrue(res);
        //}
    }
}