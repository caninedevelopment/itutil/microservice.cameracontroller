﻿using System.Collections.Generic;
using ITUtil.Common.Base;
using ITUtil.Common.Command;

namespace Monosoft.Service.CameraController.v1.Command.CameraController
{

        public class Namespace : INamespace
        {
            public static Claim admin = new Claim() { key = "CameraControler.isAdmin", dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("EN", "User is administrator") } };

            public string Name
            {
                get
                {
                    return "CameraController";
                }
            }

            public List<ICommandBase> Commands
            {
                get
                {
                    return new List<ICommandBase>()
                    {
                    };
                }

                set
                {
                    ;
                }
            }

            public ProgramVersion ProgramVersion
            {
                get
                {
                    return new ProgramVersion("1.0.0.1");
                }
            }
        }
    }
