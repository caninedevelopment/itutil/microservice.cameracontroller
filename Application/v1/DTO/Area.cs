using System.Collections.Generic;

namespace Monosoft.Service.CameraController.v1.DTO
{

    public class Polygon
    {
        public List<Point> points { get; set; }
    }
    public class Point
    {
        public int x { get; set; }
        public int y { get; set; }
    }
}
