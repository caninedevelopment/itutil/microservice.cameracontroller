using System.Collections.Generic;
using System.Linq;

namespace CameraHttpTrigger
{
    public class Polygon
    {
        public List<Point> points { get; set; }
        public Area Convert2Area()
        {
            int minX = points.Min(p => p.x);
            int maxX = points.Max(p => p.x);
            int minY = points.Min(p => p.y);
            int maxY = points.Max(p => p.y);

            return new Area() { topLeftX = minX, topLeftY = minY, height = maxY - minY, width = maxX - minX };
        }
    }
    public class Point
    {
        public int x { get; set; }
        public int y { get; set; }
    }

    public class Area
    {
        public int topLeftX { get; set; }
        public int topLeftY { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }
}
