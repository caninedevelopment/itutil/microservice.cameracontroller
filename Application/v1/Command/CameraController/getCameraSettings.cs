﻿using ITUtil.Common.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.CameraController.v1.Command.CameraController
{
    public class getCameraSettings : CommandBase<DTO.CameraRequest, DTO.CameraSettings>
    {
        public override string Description { get { return "Returns the camera settings"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.admin }; } }

        public override DTO.CameraSettings Execute(DTO.CameraRequest input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            return logic.GetSettings(input);
        }
    }
}
