using System;

namespace CameraHttpTrigger
{
    public enum TriggerType { FaceDetection, HumanDetection, VechileDetection}
    public class Trigger
    {
        public Polygon polygon { get; set; }
        public TriggerType detectionType { get; set; }
        public double treshold { get; set; }
    }
}
