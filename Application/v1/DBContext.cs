using System;
using Monosoft.Service.CameraController.v1.DTO;
using System.Linq;
using System.Collections.Generic;
using ServiceStack.OrmLite;
using ITUtil.Common.Config;

namespace Monosoft.Service.CameraController.v1
{
    public class DBContext
    {
        private static OrmLiteConnectionFactory dbFactory = null;
        private static string dbName = "cameracontroller";
        public DBContext(SQLSettings settings)
        {
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings);

            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, dbName);
            }

            // set the factory up to use the microservice database
            dbFactory = GetConnectionFactory(settings, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<v1.Database.CameraController>();
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServerDialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }

        public void NewSettings(CameraSettings input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<v1.Database.CameraController>(input.id);
                if (dbresult == null)
                {
                    db.Insert<v1.Database.CameraController>(new v1.Database.CameraController() { Id= input.id, CameraSettings = input });
                }
                else
                {
                    dbresult.CameraSettings = input;
                    db.Update<v1.Database.CameraController>(dbresult);
                }
            }
        }
        public CameraSettings GetSettingsFromId(Guid id)
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<v1.Database.CameraController>(id);
                if (dbresult != null)
                {
                    return dbresult.CameraSettings;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<CameraSettings> GetAll()
        {
            List<CameraSettings> settings = new List<CameraSettings>();

            using (var db = dbFactory.Open())
            {
                return db.Select<v1.Database.CameraController>().Select(p=>p.CameraSettings).ToList();
            }
        }
    }
}