//namespace Monosoft.Service.CameraController.v1
//{
//    using System.Collections.Generic;
//    using Monosoft.Service.CameraController.v1.DTO;
//    using ITUtil.Common.DTO;
//    using ITUtil.Common.Utils;
//    using ITUtil.Common.Utils.Bootstrapper;
//    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

//    public class Controller : IMicroservice
//    {
//        private static Logic logic = new Logic();
        
//        public string ServiceName { get { return "CameraController"; } }

//        public ProgramVersion ProgramVersion { get { return new ProgramVersion("1.0.0.0"); } }

//        public static MetaDataDefinition admin = new MetaDataDefinition("CameraController", "admin", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("EN", "User is administrator"));
//        public List<OperationDescription> OperationDescription
//        {
//            get
//            {
//                return new List<OperationDescription>()
//                {
//                    new OperationDescription(
//                        "setCameraSettings",
//                        "Sets the ressolution and framrate of the camera",
//                        typeof(CameraSettings),
//                        null,
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        SetCameraSettings
//                    ),
//                    new OperationDescription(
//                        "getCameraSettings",
//                        "Returns the camera settings",
//                        typeof(CameraRequest),
//                        typeof(CameraSettings),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        GetCameraSettings
//                    ),
//                };
//            }
//        }
//        public static ReturnMessageWrapper SetCameraSettings(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraSettings>(wrapper.messageData);
//            logic.SetSettings(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper GetCameraSettings(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraRequest>(wrapper.messageData);
//            CameraSettings message = logic.GetSettings(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, message, LocalizedString.OK);
//        }
//    }
//}
