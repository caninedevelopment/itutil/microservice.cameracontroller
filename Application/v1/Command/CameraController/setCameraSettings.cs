﻿using ITUtil.Common.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.CameraController.v1.Command.CameraController
{
    public class setCameraSettings : CommandBase<DTO.CameraSettings, object>
    {
        public override string Description { get { return "Sets the ressolution and framrate of the camera"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.admin }; } }

        public override object Execute(DTO.CameraSettings input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            logic.SetSettings(input);
            return null;
        }
    }

}
