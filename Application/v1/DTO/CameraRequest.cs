using System;

namespace Monosoft.Service.CameraController.v1.DTO
{
    public class CameraRequest
    {
        public Guid camID { get; set; }
    }
}
