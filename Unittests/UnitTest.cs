using NUnit.Framework;
using System;
using System.Collections.Generic;
using Monosoft.Service.CameraController.v1.DTO;
using Monosoft.Service.CameraController.v1;
using ITUtil.Common.DTO;

namespace Unittests
{
    [TestFixture]
    public class CameraController
    {
        [Test]
        public void CameraFeedNoTriggerTest()
        {
            Logic logic = new Logic();
            string camID = "0C7DC60D-AC7A-4879-A0A7-43E965837F7D";

            CameraSettings camReq = new CameraSettings();
            camReq.id = new Guid(camID);
            camReq.url = new ResponseStreamUri()
            {
                uri = "https://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=25&fps=5",
                username = "root",
                password = "1234"
            };
            camReq.triggerSettings = new TriggerSetting()
            {
                secondsAfterTriggerInFeedMode = 0,
                secondsBeforeTriggerInFeedMode = 0,
                triggerDelayInMiliseconds = 1000
            };
            camReq.camTriggerData = new List<Trigger>();
            camReq.camTriggerData.Add(new Trigger() { detectionType= TriggerType.FaceDetection, polygon=null, treshold=1 });

            bool t = logic.StartCameraFeed(camReq);

            Assert.IsFalse(t, "Camfeed started with no triggers");
        }
    //    [Test]
    //    public void CameraFeedTest()
    //    {
    //        Logic logic = new Logic();
    //        Database db = new Database();
    //        string camID = "0C7DC60D-AC7A-4879-A0A7-43E965837F7D";

    //        AddTriggerRequest triggerReq = new AddTriggerRequest();
    //        triggerReq.camID = new Guid(camID);
    //        triggerReq.trigger = new Trigger();
    //        triggerReq.trigger.detectionType = TriggerType.FaceDetection;
    //        triggerReq.trigger.treshold = 0.03;
    //        Polygon a = new Polygon();
    //        a.Points = new List<Point>();
    //        a.Points.Add(new Point() { x = 0, y = 0 });
    //        a.Points.Add(new Point() { x =800, y = 600 });
    //        triggerReq.trigger.polygon = a;

    //        CameraSettings camSet = new CameraSettings();

    //        camSet.id = new Guid(camID);
    //        camSet.URL = new ResponseStreamUri();
    //        camSet.URL.uri = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";

    //        logic.SetSettings(camSet);
    //        logic.AddTrigger(triggerReq);


    //        CameraRequest camReq = new CameraRequest();
    //        camReq.camID = new Guid(camID);

    //        bool t = logic.StartCameraFeed(camReq);

    //        db.DeleteDatabase();

    //        Assert.IsTrue(t, "Cam feed didnt start");
    //    }
    //    [Test]
    //    public void CameraFeedAlreadyRunningTest()
    //    {
    //        Logic logic = new Logic();
    //        Database db = new Database();
    //        string camID = "0C7DC60D-AC7A-4879-A0A7-43E965837F7D";

    //        AddTriggerRequest triggerReq = new AddTriggerRequest();
    //        triggerReq.camID = new Guid(camID);
    //        triggerReq.trigger = new Trigger();
    //        triggerReq.trigger.detectionType = TriggerType.FaceDetection;

    //        logic.AddTrigger(triggerReq);

    //        CameraRequest camReq = new CameraRequest();
    //        camReq.camID = new Guid(camID);

    //        bool t = logic.StartCameraFeed(camReq);
    //        t = logic.StartCameraFeed(camReq);

    //        db.DeleteDatabase();

    //        Assert.IsFalse(t, "Multiple of same cam started");
    //    }
    //    [Test]
    //    public void RestartFromFileTest()
    //    {
    //        Logic logic = new Logic();
    //        bool t = false;
    //        Database db = new Database();
    //        string camID = "0C7DC60D-AC7A-4879-A0A7-43E965837F7D";

    //        AddTriggerRequest triggerReq = new AddTriggerRequest();
    //        triggerReq.camID = new Guid(camID);
    //        triggerReq.trigger = new Trigger();
    //        triggerReq.trigger.detectionType = TriggerType.FaceDetection;

    //        logic.AddTrigger(triggerReq);

    //        CameraRequest camReq = new CameraRequest();
    //        camReq.camID = new Guid(camID);
    //        logic.StartCameraFeed(camReq);

    //        Thread.Sleep(5);

    //        logic.runningCams[0].camAplication.Kill();
    //        logic.runningCams.RemoveAt(0);

    //        RestartLog ids = logic.RestartFromFile();

    //        if(ids.camIDs[0] == new Guid(camID))
    //        {
    //            t = true;
    //        }

    //        logic.runningCams[0].camAplication.Kill();
    //        logic.runningCams.RemoveAt(0);
    //        db.DeleteDatabase();
    //        File.Delete(Directory.GetCurrentDirectory() + "\\RunningCams.txt");
    //        Assert.IsTrue(t, "Cam feed didnt start");
    //    }
    //}
    //[TestFixture]
    //public class CameraControllerDB
    //{
    //    Logic logic = new Logic();

    //    //[Test]
    //    //public void DBTest()
    //    //{
    //    //    bool t = false;
    //    //    string output = "";
    //    //    string camID = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";


    //    //    TriggerData trigData = new TriggerData();
    //    //    trigData.camID = new Guid(camID);
    //    //    trigData.camTriggerData = new List<Trigger>();

    //    //    Trigger trig = new Trigger();
    //    //    trig.detectionType = TriggerType.FaceDetection;

    //    //    trigData.camTriggerData.Add(trig);

    //    //    Database db = new Database();

    //    //    db.Create(trigData);
    //    //    List<TriggerData> TD = db.ListAll();

    //    //    if (TD[0].camID == new Guid(camID))
    //    //    {
    //    //        output += "Create = ok, GetAll = ok, ";
    //    //    }

    //    //    TriggerData data = TD[0];
    //    //    data.camTriggerData.Add(trig);
    //    //    db.Update(data.camID, data);

    //    //    TriggerData T = db.GetFromId(new Guid(camID));

    //    //    if (T.camTriggerData.Count == 2)
    //    //    {
    //    //        output += "Update = ok, GetFromID = ok, ";
    //    //    }

    //    //    db.Remove(new Guid(camID));

    //    //    TD = db.ListAll();

    //    //    if (TD.Count <= 0)
    //    //    {
    //    //        output += "Remove = ok";
    //    //    }

    //    //    if (output == "Create = ok, GetAll = ok, Update = ok, GetFromID = ok, Remove = ok")
    //    //    {
    //    //        t = true;
    //    //    }


    //    //    db.DeleteDatabase();

    //    //    Assert.IsTrue(t, "CameraDB test true");
    //    //}
    //    //[Test]
    //    //public void AddTriggerTest()
    //    //{
    //    //    bool t = false;
    //    //    Database db = new Database();
    //    //    string camID = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //    //    AddTriggerRequest triggerReq = new AddTriggerRequest();
    //    //    triggerReq.camID = new Guid(camID);
    //    //    triggerReq.trigger = new Trigger();
    //    //    triggerReq.trigger.detectionType = TriggerType.FaceDetection;

    //    //    logic.AddTrigger(triggerReq);
    //    //    logic.AddTrigger(triggerReq);

    //    //    List<TriggerData> data = db.ListAll();

    //    //    if(data[0].camID == new Guid(camID) && data.Count == 1)
    //    //    {
    //    //        t = true;
    //    //    }

    //    //    db.DeleteDatabase();
    //    //    Assert.IsTrue(t, "Add trigger test faild");
    //    //}
    //    //[Test]
    //    //public void RemoveTriggerTest()
    //    //{
    //    //    bool t = false;
    //    //    string output = "";
    //    //    Database db = new Database();
    //    //    string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //    //    string triggerID1 = "05BE2185-BE4F-49CC-A6F7-E3CDDB67998C";
    //    //    string triggerID2 = "E3A82AB1-1676-400C-9BF1-28558DFA7D71";

    //    //    AddTriggerRequest triggerReq = new AddTriggerRequest();
    //    //    triggerReq.camID = new Guid(camID1);
    //    //    triggerReq.trigger = new Trigger();
    //    //    triggerReq.trigger.detectionType = TriggerType.FaceDetection;
    //    //    triggerReq.trigger.id = new Guid(triggerID1);
    //    //    logic.AddTrigger(triggerReq);

    //    //    triggerReq.trigger.id = new Guid(triggerID2);
    //    //    logic.AddTrigger(triggerReq);

    //    //    RemoveTriggerRequest removeReq = new RemoveTriggerRequest();
    //    //    removeReq.camID = new Guid(camID1);
    //    //    removeReq.triggerID = new Guid(triggerID1);

    //    //    logic.RemoveTrigger(removeReq);

    //    //    TriggerData data = db.GetFromId(new Guid(camID1));

    //    //    if(data.camTriggerData.Count == 1)
    //    //    {
    //    //        output += "RemoveTrigger = ok";
    //    //    }

    //    //    removeReq.triggerID = new Guid(triggerID2);
    //    //    logic.RemoveTrigger(removeReq);

    //    //    if(output == "RemoveTrigger = ok")
    //    //    {
    //    //        t = true;
    //    //    }


    //    //    db.DeleteDatabase();
    //    //    Assert.IsTrue(t, "Remove trigger test faild");
    //    //}
    //    //[Test]
    //    //public void SaveSettingsTest()
    //    //{
    //    //    bool t = false;
    //    //    Database db = new Database();
    //    //    string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //    //    CameraRequest req = new CameraRequest();
    //    //    req.camID = new Guid(camID1);

    //    //    CameraSettings camSet = new CameraSettings();

    //    //    camSet.id = new Guid(camID1);
    //    //    camSet.ipURL = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";

    //    //    logic.SetSettings(camSet);

    //    //    CameraSettings camsettings = db.GetSettingsFromId(req.camID);

    //    //    if(camsettings.fps == 5)
    //    //    {
    //    //        t = true;
    //    //    }

    //    //    db.DeleteDatabase();
    //    //    Assert.IsTrue(t, "SaveSettingsFaild");
    //    //}
    //    [Test]
    //    public void SaveSettingsThenLoadTriggerTest()
    //    {
    //        bool t = false;
    //        Database db = new Database();
    //        string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //        CameraSettings camSet = new CameraSettings();

    //        camSet.id = new Guid(camID1);
    //        camSet.URL = new ResponseStreamUri();
    //        camSet.URL.uri = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";

    //        logic.SetSettings(camSet);

    //        TriggerData data = db.GetFromId(new Guid(camID1));

    //        if (data.camTriggerData.Count == 0)
    //        {
    //            t = true;
    //        }

    //        db.DeleteDatabase();
    //        Assert.IsTrue(t);
    //    }
    //    [Test]
    //    public void SaveSettingsTriggersAddedTest()
    //    {
    //        bool t = false;
    //        Database db = new Database();
    //        string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //        string triggerID1 = "05BE2185-BE4F-49CC-A6F7-E3CDDB67998C";

    //        AddTriggerRequest triggerReq = new AddTriggerRequest();
    //        triggerReq.camID = new Guid(camID1);
    //        triggerReq.trigger = new Trigger();
    //        triggerReq.trigger.detectionType = TriggerType.FaceDetection;
    //        triggerReq.trigger.id = new Guid(triggerID1);
    //        logic.AddTrigger(triggerReq);

    //        CameraSettings camSet = new CameraSettings();

    //        var url = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";
    //        camSet.id = new Guid(camID1);
    //        camSet.URL = new ResponseStreamUri();
    //        camSet.URL.uri = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";

    //        camSet.triggerSettings = new TriggerSetting();
    //        camSet.triggerSettings.maxFpsinPictureMode = 1;
    //        camSet.triggerSettings.secondsAfterTriggerInFeedMode = 1;

    //        logic.SetSettings(camSet);

    //        CameraSettings data = db.GetSettingsFromId(new Guid(camID1));

    //        if (data != null && data.URL.uri == url)
    //        {
    //            t = true;
    //        }

    //        db.DeleteDatabase();
    //        Assert.IsTrue(t);
    //    }
    //    //[Test]
    //    //public void LoadSettingsWih1TriggerInDBTest()
    //    //{
    //    //    bool t = false;
    //    //    Database db = new Database();
    //    //    string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //    //    string triggerID1 = "05BE2185-BE4F-49CC-A6F7-E3CDDB67998C";

    //    //    AddTriggerRequest triggerReq = new AddTriggerRequest();
    //    //    triggerReq.camID = new Guid(camID1);
    //    //    triggerReq.trigger = new Trigger();
    //    //    triggerReq.trigger.detectionType = TriggerType.FaceDetection;
    //    //    triggerReq.trigger.id = new Guid(triggerID1);
    //    //    logic.AddTrigger(triggerReq);

    //    //    CameraSettings data = db.GetSettingsFromId(new Guid(camID1));

    //    //    if (data != null && data.fps == 5)
    //    //    {
    //    //        t = true;
    //    //    }
    //    //    db.DeleteDatabase();
    //    //    Assert.IsTrue(t);
    //    //}
    //    //[Test]
    //    //public void LoadSettings()
    //    //{
    //    //    bool t = false;
    //    //    Database db = new Database();
    //    //    string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";

    //    //    CameraSettings camSet = new CameraSettings();

    //    //    camSet.bitrate = 0;
    //    //    camSet.brand = "axis";
    //    //    camSet.compression = 30;
    //    //    camSet.fps = 5;
    //    //    camSet.id = new Guid(camID1);
    //    //    camSet.ip = "192.168.1.133";
    //    //    camSet.ipURL = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";
    //    //    camSet.keyframeInterval = 32;
    //    //    camSet.protocol = "http";
    //    //    camSet.resolutionX = 800;
    //    //    camSet.resolutionY = 600;

    //    //    logic.SetSettings(camSet);

    //    //    CameraRequest req = new CameraRequest();
    //    //    req.camID = new Guid(camID1);

    //    //    CameraSettings data = logic.GetSettings(req);

    //    //    if (data != null && data.fps == 5)
    //    //    {
    //    //        t = true;
    //    //    }

    //    //    db.DeleteDatabase();
    //    //    Assert.IsTrue(t);
    //    //}
    //    //[Test]
    //    //public void SetSettingsAddTriggerRemoveTriggerNotDelete()
    //    //{
    //    //    bool t = false;
    //    //    Database db = new Database();
    //    //    string camID1 = "65F7003E-1E9D-4D15-8CB5-696873F33CBD";
    //    //    string triggerID1 = "05BE2185-BE4F-49CC-A6F7-E3CDDB67998C";

    //    //    CameraSettings camSet = new CameraSettings();

    //    //    camSet.id = new Guid(camID1);
    //    //    camSet.ipURL = "http://192.168.1.133/mjpg/video.mjpg?resolution=800x600&compression=30&mirror=0&rotation=0&textsize=small&textposition=top&textbackgroundcolor=black&textcolor=white&text=0&clock=0&date=0&overlayimage=0&fps=5&keyframe_interval=32&videobitrate=0";

    //    //    logic.SetSettings(camSet);

    //    //    AddTriggerRequest triggerReq = new AddTriggerRequest();
    //    //    triggerReq.camID = new Guid(camID1);
    //    //    triggerReq.trigger = new Trigger();
    //    //    triggerReq.trigger.detectionType = TriggerType.FaceDetection;

    //    //    logic.AddTrigger(triggerReq);

    //    //    RemoveTriggerRequest req = new RemoveTriggerRequest();
    //    //    req.camID = new Guid(camID1);
    //    //    req.triggerID = new Guid(triggerID1);

    //    //    logic.RemoveTrigger(req);

    //    //    CameraRequest camreq = new CameraRequest();
    //    //    camreq.camID = new Guid(camID1);

    //    //    CameraSettings data = logic.GetSettings(camreq);

    //    //    if (data != null && data.fps == 5)
    //    //    {
    //    //        t = true;
    //    //    }

    //    //    db.DeleteDatabase();
    //    //    Assert.IsTrue(t);
    //    //}
    }
}