﻿using System;
using System.Collections.Generic;

namespace CameraHttpTrigger.DTO
{
    public class TriggerEvent
    {
        public List<Trigger> onTriggers { get; set; }
        public string base64Frame { get; set; }
        public Guid cameraId { get; set; }

    }

}
